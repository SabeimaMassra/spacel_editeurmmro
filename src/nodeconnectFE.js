import React from 'react';
import $ from 'jquery';
class PersonComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "testname",
      lastName: "testlastname",
      firstName: "testname",
    }
  }

  componentDidMount() {
    this.fetch();
  }

  fetch() {
    var context = this;

    $.ajax({
      url: 'http://localhost:3000',
      method: 'GET',
      success: function(response) {
        context.setState({
          firstName: response.firstName,
          lastName: response.lastName
        });
      }
    });
  }

  render() {
    return (
      <div>
        <h1>{this.state.firstName} {this.state.lastName}</h1>
      </div>
    );
  }
}
export default  PersonComponent ;

