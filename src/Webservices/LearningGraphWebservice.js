var request = require('request');
var querystring = require('querystring');
const bodyParser = require('body-parser');
const xml2js = require('xml2js');
const express = require('express')
const cors = require('cors');
const app = express()
const port = 3000
var hostname  = '127.0.0.1';
app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(express.json());
app.use(cors());
var bodyContent;
var listlo;
var listloWithPre;
var ListofPresqOfLo;
var LosNoPrereq;
var ListTraining;
var JsonBodyLP;
app.use(bodyParser.urlencoded({ extended: true }));


var LearningGR = querystring.stringify({query:
 " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
+ "PREFIX owl: <http://www.w3.org/2002/07/owl#> " 
+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " 
+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
 + "PREFIX traino: <http://linc.iut.univ-paris8.fr/learningCafe/Training.owl>"

 //?training  ?idpreviouslo  ?previoustitlelo ?idnextlo ?nextlearningObject  ?nexttitlelo
//?pr ?idPedagogicalResource ?titlePedagogicalResource ?levelPResource ?urlPResource
//?durationPResource ?formatPedagogicalResource ?languagePedagogicalResource
		+ "JSON {'training': ?training,'idpreviouslo': ?idpreviouslo, 'previoustitlelo': ?previoustitlelo , 'idnextlo':?idnextlo , 'nexttitlelo':?nexttitlelo ,'Ressource':?titlePedagogicalResource ,'idRessource':?idPedagogicalResource,"
		+ "'titreRessource':?titlePedagogicalResource,'levelRessource':?levelPResource,'urlRessource':?urlPResource,"
	    + "'dureeRessource':?durationPResource,'formatRessource':?formatPedagogicalResource,"
	    + "'langRessource':?languagePedagogicalResource}"
		+ "WHERE { "
		+ "	?trade rdf:type traino:Trade ."
		+ "?training traino:hasTrade ?trade ." 
		+ "?training rdf:type traino:Training ."
		+ " ?training traino:idTraining 'DEV'^^xsd:string." 
		+ "?training traino:hasLearningObject ?previouslo ."
		+ "	 ?previouslo traino:idLearningObject ?idpreviouslo ."
		+ "?previouslo  rdf:type traino:LearningObject ."
		+ " ?nextlo traino:hasPrerequisiteLearningObject ?previouslo ."
		+ " ?nextlo rdf:type traino:LearningObject ."
		+ "  ?nextlo traino:idLearningObject ?idnextlo ."
		+ "?nextlo  traino:titleLearningObject ?nexttitlelo ."
		+ "	?previouslo traino:titleLearningObject ?previoustitlelo ."
		+ "   ?previouslo traino:hasPedagogicalResource ?pr ."
		+ "	  ?pr traino:idPedagogicalResource ?idPedagogicalResource ."
		+ "  ?pr traino:titlePedagogicalResource ?titlePedagogicalResource ."
		+ "  optional { ?pr  traino:levelPedagogicalResource ?levelPedagogicalResource ."
			+ "	 ?pr  traino:urlPedagogicalResource ?urlPedagogicalResource ."
			+ "  ?pr  traino:durationPedagogicalResource ?durationPedagogicalResource ."
			+ "   ?pr  traino:formatPedagogicalResource ?formatPedagogicalResource ."
			+ "   ?pr  traino:languagePedagogicalResource ?languagePedagogicalResource .  }"
			+ "	bind(str(?levelPedagogicalResource) as ?levelPResource) . "
			+ "	bind(str(?urlPedagogicalResource) as ?urlPResource) . "
			+ "bind(str(?durationPedagogicalResource) as ?durationPResource) . "
			
			+ "} "
			+ "order by  ?training ?idpreviouslo "
		   
		
}) ;
 
request.post(
    
    {
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        url:'http://localhost:3030/testTrainoRdf/?'+LearningGR
    },
    function (error, response, body) {
     if (!error && response.statusCode == 200) {
   
   // console.log('successful connection to Fesuki');
	
	bodyContent=response.body;
	//console.log('body content :'+bodyContent);
  } 
  else
  {
   console.log(response.statusCode)
   console.warn(error);
  
  }
});


var ListofLos = querystring.stringify({query:
	" PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
   + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
   + "PREFIX owl: <http://www.w3.org/2002/07/owl#> " 
   + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " 
   + "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
	+ "PREFIX traino: <http://linc.iut.univ-paris8.fr/learningCafe/Training.owl>"
   
	//?training  ?idpreviouslo  ?previoustitlelo ?idnextlo ?nextlearningObject  ?nexttitlelo
	//?pr ?idPedagogicalResource ?titlePedagogicalResource ?levelPResource ?urlPResource
			   //	?durationPResource ?formatPedagogicalResource ?languagePedagogicalResource
		 + "JSON {'idPreviouslevel': ?idPreviouslevel ,'Previouslevel': ?Previouslevel ,'idcurrlevello':?idcurrlevello,'idnextlo':?idnextlo "
		  + "}" 
		    +"WHERE { "
		   + "?trade rdf:type traino:Trade ."
		   + "?training traino:hasTrade ?trade ." 
		   + "?training rdf:type traino:Training ."
		   + "?training traino:idTraining 'DEV'^^xsd:string." 
		   + "?training traino:hasLearningObject ?previouslo ."
		   + "?previouslo traino:idLearningObject ?idcurrlevello ."
		   + "?previouslo  rdf:type traino:LearningObject ."
		   + "?nextlo traino:hasPrerequisiteLearningObject ?previouslo ."
		   +" ?Previouslevel ^traino:hasPrerequisiteLearningObject ?previouslo ."
		   +"?Previouslevel   traino:idLearningObject ?idPreviouslevel ."
		   + "?nextlo rdf:type traino:LearningObject . "
		   + "?nextlo traino:idLearningObject ?idnextlo ."
		   + "} "
		    + "order by ?Previouslevel "
		  
   }) ;

request.post(
    
    {
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        url:'http://localhost:3030/trainoturtl/?'+ListofLos
    },
    function (error, response, body) {
     if (!error && response.statusCode == 200) {
   
    console.log('successful connection to Fesuki : ListofLos');
	listlo=response.body;
	console.log('ListofLos :'+listlo);

  } 
  else
  {
   console.log(response.statusCode)
   console.warn(error);
  
  }
});

var ListofCurLosWithPresq = querystring.stringify({query:
	" PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
   + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
   + "PREFIX owl: <http://www.w3.org/2002/07/owl#> " 
   + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " 
   + "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
	+ "PREFIX traino: <http://linc.iut.univ-paris8.fr/learningCafe/Training.owl>"
   
	//?training  ?idpreviouslo  ?previoustitlelo ?idnextlo ?nextlearningObject  ?nexttitlelo
	//?pr ?idPedagogicalResource ?titlePedagogicalResource ?levelPResource ?urlPResource
			   //	?durationPResource ?formatPedagogicalResource ?languagePedagogicalResource
		 + "JSON { 'idcurrlevello':?idcurrlevello "
		  + "}" 
		    +"WHERE { "
		   + "?trade rdf:type traino:Trade ."
		   + "?training traino:hasTrade ?trade ." 
		   + "?training rdf:type traino:Training ."
		   + "?training traino:idTraining 'DEV'^^xsd:string." 
		   + "?training traino:hasLearningObject ?previouslo ."
		   + "?previouslo traino:idLearningObject ?idcurrlevello ."
		  
		   + "} "
		    + "  group by ?idcurrlevello order by ?idcurrlevello"
		  
   }) ;
   request.post(
    
    {
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        url:'http://localhost:3030/trainoturtl/?'+ListofCurLosWithPresq
    },
    function (error, response, body) {
     if (!error && response.statusCode == 200) {
   
    console.log('successful connection to Fesuki : ListofCurLosWithPresq');
	listloWithPre=response.body;
	console.log('ListofCurLosWithPresq :'+ListofCurLosWithPresq);

  } 
  else
  {
   console.log(response.statusCode)
   console.warn(error);
  
  }
});

//list of learning object with no prerequisite LOS but are prerequist
var LosNoPrereq = querystring.stringify({query:
	" PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
   + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
   + "PREFIX owl: <http://www.w3.org/2002/07/owl#> " 
   + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " 
   + "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
	+ "PREFIX traino: <http://linc.iut.univ-paris8.fr/learningCafe/Training.owl>"
   
		 + "JSON { 'idcurrlevello':?idcurrlevello ,'idnextlo':?idnextlo "
		  + "}" 
		    +"WHERE { "
			+" ?trade rdf:type traino:Trade . "
			+"?training traino:hasTrade ?trade ."
			+"?training rdf:type traino:Training ."
			+"?training traino:idTraining 'DEV'^^xsd:string."
			+"?training traino:hasLearningObject ?previouslo ."
			+"?previouslo traino:idLearningObject ?idcurrlevello ."
			+"?previouslo  rdf:type traino:LearningObject ."
			+"?nextlo traino:hasPrerequisiteLearningObject ?previouslo ."
			+"	MINUS {  "
				+"	?Previouslevel ^traino:hasPrerequisiteLearningObject ?previouslo  ."
				+"	?Previouslevel  traino:idLearningObject ?idPreviouslevel ."
				+" } "
				+"	?nextlo rdf:type traino:LearningObject ."
				+"	?nextlo traino:idLearningObject ?idnextlo ."

		   + "} "
		    + "  order by ?idcurrlevello"
		  
   }) ;
   request.post(
    
    {
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        url:'http://localhost:3030/trainoturtl/?'+LosNoPrereq
    },
    function (error, response, body) {
     if (!error && response.statusCode == 200) {
   
    // console.log('successful connection to Fesuki : LosNoPrereq');
	LosNoPrereq=response.body;
	//console.log('LosNoPrereq :'+LosNoPrereq);

  } 
  else
  {
   console.log(response.statusCode)
   console.warn(error);
  
  }
});

//get list of all training 
var ListTrainingQuery = querystring.stringify({query:
	" PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
   + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
   + "PREFIX owl: <http://www.w3.org/2002/07/owl#> " 
   + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " 
   + "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
	+ "PREFIX traino: <http://linc.iut.univ-paris8.fr/learningCafe/Training.owl>"
   
		 + "JSON { 'idtraining':?idtraining  "
		  + "}" 
		    +"WHERE { ?trade rdf:type traino:Trade ." 
			+ "	?training traino:hasTrade ?trade ."
			+ "?training rdf:type traino:Training ."
			+ "	?training traino:idTraining ?idtraining ."
			
		   + "} "
		 
		  
   }) ;
   request.post(
    
    {
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        url:'http://localhost:3030/trainoturtl/?'+ListTrainingQuery
    },
    function (error, response, body) {
     if (!error && response.statusCode == 200) {
   
    console.log('successful connection to Fesuki : ListTraining');
	ListTraining=response.body;
	console.log('ListTraining :'+ListTraining);

  } 
  else
  {
   console.log(response.statusCode)
   console.warn(error);
  
  }
});


app.get('/LosNoPrereq', (req, res) => {
	console.log('LosNoPrereq successful get');
	
   res.json(LosNoPrereq)
  
  })


app.get('/ListofCurLosWithPresq', (req, res) => {
	console.log('ListofCurLosWithPresq successful get');
	
    res.json(listloWithPre)
  
  })
    
app.get('/LearningGraph', (req, res) => {
console.log('LearningGraph successful get');
  
res.json(bodyContent)

})

//get all training available 
app.get('/ListTraining', (req, res) => {
console.log('ListTraining successful get '+ListTraining);
	
res.json(ListTraining)
  
  })

//get the list of a pedagogical resources of a learing object
var LoSelected;
  app.post('/getPrs', function(req, res) {
	LoSelected=req.body.nameLo
	console.log('req data response name  '+req.body.nameLo); 
	
	//res.send(req.body);
	//res.send('this is a response message from get pedagogocal ressouces ')
	//the response at last step
	//res.send(ListofPresqOfLo)
	//request where to add the data sent to get app (the lo selected)


	var ListofPresqOfLo = querystring.stringify({query:
		" PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
	   + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
	   + "PREFIX owl: <http://www.w3.org/2002/07/owl#> " 
	   + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " 
	   + "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
	   + "PREFIX traino: <http://linc.iut.univ-paris8.fr/learningCafe/Training.owl#>"
		+ "JSON {'idRessource':?idPedagogicalResource,"
			+ "'titreRessource':?titlePedagogicalResource,'levelRessource':?levelPResource,'urlRessource':?urlPResource,"
			+ "'dureeRessource':?durationPResource,'formatRessource':?formatPedagogicalResource,"
			+ "'langRessource':?languagePedagogicalResource}"  
				+"WHERE { "  
				+"?trade rdf:type traino:Trade . " 
				+"?training traino:hasTrade ?trade ." 
				+"?training rdf:type traino:Training ."
				+"?training traino:idTraining 'DEV' ."
				+"?training traino:hasLearningObject ?lo ."
				+"?lo traino:idLearningObject '"+LoSelected+"' ." 
				+"?lo  rdf:type traino:LearningObject ."
				+"?lo traino:hasPedagogicalResource ?pr ."
				+"?pr traino:idPedagogicalResource ?idPedagogicalResource ."
				+"?pr traino:titlePedagogicalResource ?titlePedagogicalResource ."
				+"  optional { ?pr  traino:levelPedagogicalResource ?levelPedagogicalResource ."
					+"	?pr  traino:urlPedagogicalResource ?urlPedagogicalResource ."
					+" ?pr  traino:durationPedagogicalResource ?durationPedagogicalResource ."
					+"?pr  traino:formatPedagogicalResource ?formatPedagogicalResource ."
					+" ?pr  traino:languagePedagogicalResource ?languagePedagogicalResource .  }"
					+"	bind(str(?levelPedagogicalResource) as ?levelPResource) . "
					+" bind(str(?urlPedagogicalResource) as ?urlPResource) . "
					+"   bind(str(?durationPedagogicalResource) as ?durationPResource) . "
			   + "} "
				+ "  order by  ?levelPResource "
	   }) ;

	   //then post it in request to fesuki
	   request.post(
		
		{
			headers: {'content-type' : 'application/x-www-form-urlencoded'},
			url:'http://localhost:3030/testTrainoRdf/?'+ListofPresqOfLo
		},
	
		function (error, response, body) {
		 if (!error && response.statusCode == 200) {
	   
		//console.log('connection a fersuki liste de ressouces Pd'+ListofPresqOfLo);
		ListofPresqOfLo=response.body;
		//console.log('ListofPresqOfLo :'+ListofPresqOfLo);
		console.log('ListofPresqOfLo :'+ListofPresqOfLo);
	
	  } 
	  else
	  {
	   console.log(response.statusCode)
	   console.warn(error);
	  
	  }
	});

})



//get list des Los 
//app.get('/getlistlo', (req, res) => {
	//console.log('listlo successful get :'+listlo);
	//res.json(listlo)
   //})
//post Training to get list of LOs

var SeledtedTr
app.post('/listlo', function(req, res) {
	
	SeledtedTr=req.body.tr
	console.log('post listlo  '+SeledtedTr); 

	//SeledtedTr='DEV'
	//console.log('post listlo after  '+SeledtedTr); 
	//res.send(req.body);
	//res.send('this is a response message from get pedagogocal ressouces ')
	//the response at last step
	//res.send(ListofPresqOfLo)
	//request where to add the data sent to get app (the lo selected)

	var ListofLos = querystring.stringify({query:
		" PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
	   + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
	   + "PREFIX owl: <http://www.w3.org/2002/07/owl#> " 
	   + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " 
	   + "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
		+ "PREFIX traino: <http://linc.iut.univ-paris8.fr/learningCafe/Training.owl>"
	   
		//?training  ?idpreviouslo  ?previoustitlelo ?idnextlo ?nextlearningObject  ?nexttitlelo
		//?pr ?idPedagogicalResource ?titlePedagogicalResource ?levelPResource ?urlPResource
				   //	?durationPResource ?formatPedagogicalResource ?languagePedagogicalResource
			 + "JSON {'idPreviouslevel': ?idPreviouslevel ,'Previouslevel': ?Previouslevel ,'idcurrlevello':?idcurrlevello,'idnextlo':?idnextlo "
			  + "}" 
				+"WHERE { "
			   + "?trade rdf:type traino:Trade ."
			   + "?training traino:hasTrade ?trade ." 
			   + "?training rdf:type traino:Training ."
			   + "?training traino:idTraining '"+SeledtedTr+"' ^^xsd:string." 
			   + "?training traino:hasLearningObject ?previouslo ."
			   + "?previouslo traino:idLearningObject ?idcurrlevello ."
			   + "?previouslo  rdf:type traino:LearningObject ."
			   + "?nextlo traino:hasPrerequisiteLearningObject ?previouslo ."
			   +" ?Previouslevel ^traino:hasPrerequisiteLearningObject ?previouslo ."
			   +"?Previouslevel   traino:idLearningObject ?idPreviouslevel ."
			   + "?nextlo rdf:type traino:LearningObject . "
			   + "?nextlo traino:idLearningObject ?idnextlo ."
			   + "} "
				+ "order by ?Previouslevel "
			  
	   }) ;

	   //then post it in request to fesuki
	   request.post(
    
		{
			headers: {'content-type' : 'application/x-www-form-urlencoded'},
			url:'http://localhost:3030/trainoturtl/?'+ListofLos
		},
		function (error, response, body) {
		 if (!error && response.statusCode == 200) {
	   
		console.log('successful connection to Fesuki : ListofLos');
		listlo=response.body;
		console.log('ListofLos :'+listlo);
	
	  } 
	  else
	  {
	   console.log(response.statusCode)
	   console.warn(error);
	  
	  }
	});

})
	 

//test post to get infos from AMS
app.post('/LGtest', function(req, res) {
	
	console.log('body form AMS webservices '+req.body); 
    res.send(req.body)
})

//end of test

app.listen(port, () => {
  console.log(`port listening `)
})
