var request = require('request');
var querystring = require('querystring');
const bodyParser = require('body-parser');
const xml2js = require('xml2js');
const express = require('express')
const cors = require('cors');
const app = express()
const port = 3000
var hostname  = '127.0.0.1';
app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(express.json());
app.use(cors());
var bodyContent;
var JsonBodyLP;
app.use(bodyParser.urlencoded({ extended: true }));


var myquery = querystring.stringify({query:
 " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
+"PREFIX owl: <http://www.w3.org/2002/07/owl#> "
+"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
+"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
+"PREFIX traino: <http://linc.iut.univ-paris8.fr/learningCafe/Training.owl#>"
+"PREFIX UserProfile: <http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#>"

//+"SELECT distinct ?firstname ?IdTraining ?ValTitreDeCompetence ?VaTitreTool ?titreLP  "

+"JSON {'name': ?firstname ,'Training': ?IdTraining ,'Competence': ?ValTitreDeCompetence , "
+"'LearningPath': ?titreLP ,'Tool':?VaTitreTool }"
+"WHERE { "
+"?ind rdf:type UserProfile:Learner . "
+"?ind UserProfile:firstname 'Adil'^^xsd:string . "
+"?ind UserProfile:firstname ?Firstname ."
+"?ind UserProfile:followed ?LearningPathFollowed ."
+"?LearningPathFollowed UserProfile:hasBeenMadeForTraining ?TrainingMadeForLp ."
+"?TrainingMadeForLp UserProfile:idTraining ?IdTraining . "
+"?PositionPRInPersonalizedLearningPath UserProfile:hasPersonalizedLearningPath  ?LearningPathFollowed ."
+"?LearningPathFollowed UserProfile:titlePersonalizedLearningPath ?titreLP . "
+"?PositionPRInPersonalizedLearningPath UserProfile:hasLearningObject ?LearningObject ."
+"?PositionPRInPersonalizedLearningPath UserProfile:hasPedagogicalResource ?Resource ."
+"?LearningPathFollowed UserProfile:hasGivenCompetence ?compMaterialTool ."
 +"?ind UserProfile:hasAcquired ?compMaterialTool  ."
+"?compMaterialTool UserProfile:hasCompetence ?comp . "
+"?comp a UserProfile:Competence ."
+"?comp UserProfile:idCompetence ?o . "
 +"?comp UserProfile:titleCompetence ?titleComp . "
+"optional {?compMaterialTool UserProfile:hasTool ?tool  .}"
+" optional {?tool UserProfile:titleTool ?titreTool   .}"
+" bind(str(?Firstname) as ?firstname) . "
+" bind(str(?titleComp) as ?ValTitreDeCompetence) . "
 +"bind(str(?titreTool) as ?VaTitreTool) . "
 +"bind(str(?compMaterialTool) as ?CompMaterialTool) . "
 +"bind(str(?LearningObject) as ?learningObject) . "
  +"bind(str(?TrainingMadeForLp) as ?trainingMadeForLp) . "
  +"bind(str(?LearningPathFollowed) as ?learningPathFollowed) . "
 
+"}"});
 
var queryPortfolio=querystring.stringify({query:
  " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
  + "PREFIX owl: <http://www.w3.org/2002/07/owl#> "
  +"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
  +"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
  +"PREFIX traino: <http://linc.iut.univ-paris8.fr/learningCafe/Training.owl#>"
  +"PREFIX UserProfile: <http://linc.iut.univ-paris8.fr/learningCafe/UserProfile.owl#>"
  +"JSON {'LearningPath': ?LearningPathFollowed ,'TrainingMadeForLp': ?TrainingMadeForLp ,'Competence': ?ValTitreDeCompetence , "
  +"'VaTitreTool': ?VaTitreTool ,'LearningObject':?LearningObject,'compMaterialTool':?compMaterialTool ,'idResource':?idResource ,'idLo':?idLo }"
  +" WHERE { "
  +" ?ind rdf:type UserProfile:Learner . "
  +" ?ind UserProfile:firstname 'Adil'^^xsd:string . "
  +"?ind UserProfile:firstname ?firstname ."
  +"?ind UserProfile:followed ?LearningPathFollowed ."
  +"?LearningPathFollowed UserProfile:hasBeenMadeForTraining ?TrainingMadeForLp ."
  +"?PositionPRInPersonalizedLearningPath UserProfile:hasPersonalizedLearningPath  ?LearningPathFollowed ."
  +" ?PositionPRInPersonalizedLearningPath UserProfile:hasLearningObject ?LearningObject ."
  +"?LearningObject UserProfile:idLearningObject  ?idLearningObject ."
  +"?PositionPRInPersonalizedLearningPath UserProfile:hasPedagogicalResource ?Resource ."
  +" ?Resource UserProfile:idPedagogicalResource ?idresource ."
  +"?LearningPathFollowed UserProfile:hasGivenCompetence ?compMaterialTool ."
  +" ?ind UserProfile:hasAcquired ?compMaterialTool  ."
  +" ?compMaterialTool UserProfile:hasCompetence ?comp . "
  +"?comp a UserProfile:Competence ."
  +"?comp UserProfile:idCompetence ?o . "
  +"?comp UserProfile:titleCompetence ?titleComp . "
  +" optional {?compMaterialTool UserProfile:hasTool ?tool  .}"
  +"optional {?tool UserProfile:titleTool ?titreTool   .}"
  +" bind(str(?idLearningObject) as ?idLo) . "
  +" bind(str(?idresource) as ?idResource) . "
  +" bind(str(?titleComp) as ?ValTitreDeCompetence) . "
  +" bind(str(?titreTool) as ?VaTitreTool) . "
  
  +" }"

}) ;



request.post(
    
    {
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        url:'http://localhost:3030/SparQlUPOVF/?'+myquery
    },
    function (error, response, body) {
     if (!error && response.statusCode == 200) {
   
  console.log('successful connection to Fesuki');
	bodyContent=response.body;
  console.log(bodyContent);
	
  } 
  else
  {
   console.log(response.statusCode)
   console.warn(error);
  }
});


app.get('/portfolio', (req, res) => {
  console.log(' portfolio successful get');
  
  //  res.end(JSON.stringify(bodyContent));
 // res.send(bodyContent);
 res.json(bodyContent)

})
var LearningGR = querystring.stringify({query:
  " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
 + " PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
 + "PREFIX owl: <http://www.w3.org/2002/07/owl#> " 
 + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " 
 + "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
  + "PREFIX traino: <http://linc.iut.univ-paris8.fr/learningCafe/Training.owl#>"
 
  //+ "SELECT distinct ?title ?pr ?idPedagogicalResource ?titlePedagogicalResource ?levelPResource ?urlPResource"
 // + "?durationPResource ?formatPedagogicalResource ?languagePedagogicalResource"
        
     + "JSON {'titre': ?title ,'Ressource':?titlePedagogicalResource ,'idRessource':?idPedagogicalResource,"
     + "'titreRessource':?titlePedagogicalResource,'levelRessource':?levelPResource,'urlRessource':?urlPResource,"
       + "'dureeRessource':?durationPResource,'formatRessource':?formatPedagogicalResource,"
       + "'langRessource':?languagePedagogicalResource}"
     
              + "	WHERE { "
         + "	?trade rdf:type traino:Trade ."
         + "	?training traino:hasTrade ?trade ."
         + "?training rdf:type traino:Training ."
         + "?training traino:titleTraining ?titletraining ."
         + "?training traino:hasLearningObject ?lo ."
         +"?lo rdf:type traino:LearningObject ."
         +"?training traino:hasLearningObject ?Initlo ."
         +"?Initlo  rdf:type traino:LearningObject ."
         +"?lo traino:hasPrerequisiteLearningObject ?Initlo ."
       	 +"?lo rdf:type traino:LearningObject ."
         +"?lo traino:titleLearningObject ?title ."
         +"?lo traino:hasPedagogicalResource ?pr ."
        + "?pr traino:idPedagogicalResource ?idPedagogicalResource ."
        + "?pr traino:titlePedagogicalResource ?titlePedagogicalResource ."
        + "	optional { ?pr  traino:levelPedagogicalResource ?levelPedagogicalResource ."
            + "	?pr  traino:urlPedagogicalResource ?urlPedagogicalResource ."
          + "?pr  traino:durationPedagogicalResource ?durationPedagogicalResource ."
          + "?pr  traino:formatPedagogicalResource ?formatPedagogicalResource ."
          + "?pr  traino:languagePedagogicalResource ?languagePedagogicalResource .  }"
           + "  bind(str(?levelPedagogicalResource) as ?levelPResource) . "
           + " bind(str(?urlPedagogicalResource) as ?urlPResource) . " 
            + " bind(str(?durationPedagogicalResource) as ?durationPResource) .}  "
         + "order by asc(?levelPResource) "
 
 }) ;

request.post(
    
  {
      headers: {'content-type' : 'application/x-www-form-urlencoded'},
      url:'http://localhost:3030/testTrainoRdf/?'+queryPortfolio
  },
  function (error, response, body) {
   if (!error && response.statusCode == 200) {
 
console.log('successful connection to Fesuki');
bodyContent=response.body;
console.log(bodyContent);

} 
else
{
 console.log(response.statusCode)
 console.warn(error);
}
});


 app.get('/Portfolio', (req, res) => {
   console.log('class Portfolio.js successful get');
  
  res.json(bodyContent)

 })

app.listen(port, () => {
  console.log(`port listening  `)
})
