var request = require('request');
var querystring = require('querystring');
const bodyParser = require('body-parser');
const xml2js = require('xml2js');
const express = require('express')
const cors = require('cors');
const app = express()
const port = 3000
var hostname  = '127.0.0.1';
app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(express.json());
app.use(cors());
var bodyContent;
var listlo;
var JsonBodyLP;
app.use(bodyParser.urlencoded({ extended: true }));


var ListofPrerequisite = querystring.stringify({query:
	" PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
   + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
   + "PREFIX owl: <http://www.w3.org/2002/07/owl#> " 
   + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " 
   + "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
	+ "PREFIX traino: <http://linc.iut.univ-paris8.fr/learningCafe/Training.owl#>"
   	 + "JSON {'idPreviouslevel': ?idPreviouslevel ,'Previouslevel': ?Previouslevel ,'idcurrlevello':?idcurrlevello,'idnextlo':?idnextlo "
		  + "}" 
		    +"WHERE { "
		   + "?trade rdf:type traino:Trade ."
		   + "?training traino:hasTrade ?trade ." 
		   + "?training rdf:type traino:Training ."
		   + "?training traino:idTraining 'DEV'^^xsd:string." 
		   + "?training traino:hasLearningObject ?previouslo ."
		   + "?previouslo traino:idLearningObject ?idcurrlevello ."
		   + "?previouslo  rdf:type traino:LearningObject ."
		   + "?nextlo traino:hasPrerequisiteLearningObject ?previouslo ."
		   +" ?Previouslevel ^traino:hasPrerequisiteLearningObject ?previouslo ."
		   +"?Previouslevel   traino:idLearningObject ?idPreviouslevel ."
		   + "?nextlo rdf:type traino:LearningObject . "
		   + "?nextlo traino:idLearningObject ?idnextlo ."
		   + "} "
		    + "order by ?Previouslevel "
		  
   }) ;
request.post(
    
    {
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        url:'http://localhost:3030/testTrainoRdf/?'+ListofPrerequisite
    },
    function (error, response, body) {
     if (!error && response.statusCode == 200) {
   
    console.log('successful connection to Fesuki : ListofPrerequisite');
	listlo=response.body;
	console.log('ListofPrerequisite :'+ListofPrerequisite);

  } 
  else
  {
   console.log(response.statusCode)
   console.warn(error);
  
  }
});

app.get('/getPrerequist', (req, res) => {
  console.log('getPrerequist successful get');
  
 res.json(bodyContent)

})




app.listen(port, () => {
  console.log(`port listening `)
})
