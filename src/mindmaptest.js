import React, { useState, useEffect } from 'react';
import Portfolio from './Portfolio';
import { MarkerType } from 'react-flow-renderer';
import ReactFlow, {
  addEdge,
  MiniMap,
  Controls,
  Background,
  useNodesState,
  useEdgesState,
} from 'react-flow-renderer';
import $, { map } from 'jquery';
import {dataMap,datatest} from './initial-elements';
import {edges as edgesIni,nodes as nodesIni} from './datajs';
import { getDataGridUtilityClass } from '@mui/x-data-grid';


const onInit = (reactFlowInstance) => console.log('flow loaded:', reactFlowInstance);
var jsonnodes;



class nodestest extends React.Component {
  
fetch(){ 
   var context = this;
  


}
}

//class nodestest extends React.Component{  }
//const PortfolioMindMap = () => {
    //return (
      

var edgesdata = [
  { id: 'e1-2', source: '1', target: '2', label: 'this is an edge label' },
  { id: 'e1-3', source: '1', target: '3' },
  {
    id: 'e3-4',
    source: '3',
    target: '4',
    //animated: true,
   // label: 'animated edge',
  },
  {
    id: 'e4-5',
    source: '4',
    target: '5',
   // label: 'edge with arrow head',
   // markerEnd: {
   //   type: MarkerType.ArrowClosed,
   // },
  },
  {
    id: 'e5-6',
    source: '5',
    target: '6',
  //  type: 'smoothstep',
  //  label: 'smooth step edge',
  },
  {
    id: 'e2-7',
    source: '2',
    target: '7',
    //type: 'step',
   // style: { stroke: '#f6ab6c' },
   // label: 'a step edge',
   // animated: true,
   // labelStyle: { fill: '#f6ab6c', fontWeight: 700 },
  },
  {
    id: 'e2-8',
    source: '2',
    target: '8',
  //  type: 'smoothstep',
  //  label: 'smooth step edge',
  },
  {
    id: 'e2-9',
    source: '2',
    target: '9',
  //  type: 'smoothstep',
  //  label: 'smooth step edge',
  },
  {
    id: 'e2-10',
    source: '2',
    target: '10',
  //  type: 'smoothstep',
  //  label: 'smooth step edge',
  },
  {
    id:'e3-11',
    source: '3',
    target: '11',
    label: 'hasAcquired'
  //  type: 'smoothstep',
  //  label: 'smooth step edge',
  },
  {
    id:'e11-12',
    source: '11',
    target: '12',
    label: 'hasAcquired'
  //  type: 'smoothstep',
  //  label: 'smooth step edge',
  },
];
var data= "data test";
//
 function test()  {

  var settings = {
    "async": false,
    "crossDomain": true,
    "url": "http://localhost:3000/portfolio",
    "method": "GET",
    "headers": {
        "content-type": "application/json"
    }
};

var tmp = null;
$.ajax(settings).done(function (response) {
var json = $.parseJSON(response)
tmp=response;
console.log("json response"+json[0].ValTitreDeCompetence);

//console.log("datatest :"+datatest );

 data =[
  {
    id: '1',
    type: 'input',
    data: {
      label: (
        <>
          <strong> Portfolio of: {json[0].firstname}</strong>
        </>
      ),
    },
    position: { x: 250, y: 0 },
  },
  {
    id: '2',
    data: {
      label: (
        <>
         <strong>SAE Stage </strong>
        </>
      ),
    },
    position: { x: 100, y: 100 },
  },
  {
    id: '3',
    data: {
      label:  (
        <>
         <strong>Infos</strong>
         <dl>
         <a href="https://www.cv.fr/app/resumes/4f393735-00e9-47bd-9814-5acaa10a62cf/edit" download><dd>curriculum vitæ </dd>   </a>   
 
         </dl>
          
           
        </>
      ),
    },
    position: { x: 400, y: 100 },
    //style: {
      //background: '#D6D5E6',
      //color: '#333',
     // border: '1px solid #222138',
     // width: 180,
   // },
  },
 
  {
    id: '11',
    type: 'output',
    data: {label:
      (
        <>
         <strong>Projects Github  </strong>
         <dl>
           <a href="https://github.com/" > <dd> projet web application</dd></a>
            </dl>
        </>
      ),
      
      },
    position: { x: -250, y: 170 },
  },
  {
    id: '8',
    type: 'output',
    data:  {label:
      (
        <>
         <strong>Competences </strong>
         <dl>
           <a href="" > <dd>{json[0].ValTitreDeCompetence}</dd> </a>
          
           </dl>
        </>
      ),
      
      },
    position: { x: 200, y: 170 },
  },
  {
    id: '9',
    type: 'output',
    data: 
    { 
      label:  (
        <>
         <strong>Autres projets</strong>
         <dl>
         <a href="http://emagaud.p8-infocom.fr/wp-content/uploads/sites/8/2022/06/Portfolio1-EloisaMagaud-SAEPresenterUnProjet.pdf" download><dd>Presenter un Projet </dd>   </a>   
        
         </dl>
          
        </>
      ),
 
 
    },
    position: { x: -10, y: 170 },
  },
  {
    id: '10',
    type: 'output',
    data: 
    { 
      label:  (
        <>
         <strong>Stages</strong>
         <dl>
        <a href="" download><dd>Attestations de Stage </dd>   </a> 
         </dl>
          
        </>
      ),
    },
    position: { x: -10, y: 170 },
  },
  {
    id: '12',
    type: 'output',
    data: 
    { 
      label:  (
        <>
         <dl>
        <a href="" download><dd>Compétences du stage Aquit </dd>   </a> 
         </dl>
        </>
      ),
    },
    position: { x: -170, y: 170 },
  },
];
});

return data;
}

var res =test();
console.log("get res data:"+data); 
 

function MindmapDisplay()  {

  var context = this;
  var restsulttest=test();
 console.log (restsulttest);
   
//console.log(" windowName outside "+ window.name);

var dat=window.name;
  //console.log(" firstName"+this.state.nodesdata);
  const [nodes, setNodes, onNodesChange] = useNodesState(data);
  const [edges, setEdges, onEdgesChange] = useEdgesState(edgesdata);
  const onConnect = (params) => setEdges((eds) => addEdge(params, eds));
  return(
  <div style={{ height: 800 }}>

  <ReactFlow
    nodes={nodes}
    edges={edges}
    onNodesChange={onNodesChange}
    onEdgesChange={onEdgesChange}
    onConnect={onConnect}
    onInit={onInit}
    fitView
    attributionPosition="top-right"
  >

    <MiniMap
      nodeStrokeColor={(n) => {
        if (n.style?.background) return n.style.background;
        if (n.type === 'input') return '#0041d0';
        if (n.type === 'output') return '#ff0072';
        if (n.type === 'default') return '#1a192b';

        return '#eee';
      }}
      nodeColor={(n) => {
        if (n.style?.background) return n.style.background;

        return '#fff';
      }}
      nodeBorderRadius={2}
    />
    <Controls />
    <Background color="#aaa" gap={16} />
  </ReactFlow>
  </div>
)
 // return <h1>{greeting}</h1>;
}

export default MindmapDisplay;