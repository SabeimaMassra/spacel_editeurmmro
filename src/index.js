import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Home from './App';
import Portfolio from './Portfolio'  
import Hpage from './Hpage';
import PersonComponent from './nodeconnectFE';
import MindmapDisplay from './mindmaptest';
import LearningGraph from './LearningGraph';
import LearningGraphDraft from './LearningGraphDraft';
import RegistrationPage from './RegistrationPage'; 
import Tpage from './TrainingPage';
//import TestOfGraph from './graphDraw'; 

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
//import { BrowserRouter , Route, Link, Switch }
         //         from 'react-router-dom';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Router>
        <Switch>

        <Route exact path="/RegistrationPage" component={RegistrationPage} />
        <Route exact path="/LearningGraphDraft" component={LearningGraphDraft} />
        
        <Route exact path="/Portfolio" component={Portfolio} />
          <Route exact path="/Tpage" component={Tpage} />
          <Route exact path="/Hpage" component={Hpage} />
          <Route exact path="/LearningGraphDraft" component={LearningGraphDraft} />
          <Redirect to="/Tpage" />
        </Switch>
      </Router>
);


     