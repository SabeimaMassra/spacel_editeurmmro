
import React from 'react'  
import{Navbar,Container,Nav,NavDropdown,Form,FormControl,Button}  from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import $, { map } from 'jquery';
import { render } from "react-dom";
import MindMap from "react-mindmap";
import mapdata from "./map";
import { Tree } from 'react-tree-graph';
import { AnimatedTree } from 'react-tree-graph';
import { MarkerType } from 'react-flow-renderer';

import ReactFlow, {
  addEdge,
  MiniMap,
  Controls,
  Background,
  useNodesState,
  useEdgesState,
} from 'react-flow-renderer';

//import { nodes as initialNodes, edges as initialEdges } from './initial-elements';





const onInit = (reactFlowInstance) => console.log('flow loaded:', reactFlowInstance);

const OverviewFlow = () => {
  const [nodes, setNodes, onNodesChange] = useNodesState(nodes);
  const [edges, setEdges, onEdgesChange] = useEdgesState(nodes);
  const onConnect = (params) => setEdges((eds) => addEdge(params, eds));
    return (
    <ReactFlow
      nodes={nodes}
      edges={edges}
      onNodesChange={onNodesChange}
      onEdgesChange={onEdgesChange}
      onConnect={onConnect}
      onInit={onInit}
      fitView
      attributionPosition="top-right"
    >
      <MiniMap
        nodeStrokeColor={(n) => {
          if (n.style?.background) return n.style.background;
          if (n.type === 'input') return '#0041d0';
          if (n.type === 'output') return '#ff0072';
          if (n.type === 'default') return '#1a192b';

          return '#eee';
        }}
        nodeColor={(n) => {
          if (n.style?.background) return n.style.background;

          return '#fff';
        }}
        nodeBorderRadius={2}
      />
      <Controls />
      <Background color="#aaa" gap={16} />
    </ReactFlow>
  );
};


const styles = {
  fontFamily: "sans-serif",
  textAlign: "center"
};
const App = () => (
  <div style={styles}>
    <MindMap nodes={map.nodes} connections={map.connections} />
    <h2>Start editing to see some magic happen {"\u2728"}</h2>
  </div>
);

let data = {
	name: 'Parent',
	children: [{
		name: 'Child One'
	}, {
		name: 'Child Two' ,
    children: [{
      name: 'Child One Parent2'
    }, {
      name: 'Child Two Parent2'
    }] ,
	}] ,

};


class Portfolio extends React.Component {  

  constructor(props) {
    super(props);

    this.state = {
      firstName: " first name empty",
     
    }
  }
 
  componentDidMount() {
    this.fetch();
  }
  
   fetch() {

     const data = {
      name: 'Parent',
      children: [{
        name: 'Child One'
      }, {
        name: 'Child Two' ,
        children: [{
          name: 'Child One Parent2'
        }, {
          name: 'Child Two Parent2'
        }] ,
      }] ,
     
     
    };
    var obj = {
      firstname: 'firstname',
      IdTraining :'IdTraining',
      ValTitreDeCompetence:'ValTitreDeCompetence',
       titreLP :'titreLP',
    
    }

  
    var context = this;
    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://localhost:3000/portfolio",
      "method": "GET",
      "headers": {
          "content-type": "application/json"
      }
  };

  $.ajax(settings).done(function (response) {
    var json = $.parseJSON(response);
    console.log("json response"+json[0].firstname);
    context.setState({
      firstName: json[0].firstname,
      IdTraining :json[0].IdTraining,
      ValTitreDeCompetence:json[0].ValTitreDeCompetence,
      titreLP :json[0].titreLP,
    });
    
   console.log (" this is a portfolio test succeed"+response);
 
}
)};

render() {
 
  return(
    <div></div>
  )
  }
}  

export default Portfolio 