import { useState } from 'react';
import React from 'react'  
import Form from 'react-bootstrap/Form';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import{Navbar,Container,Nav,NavDropdown,FormControl}  from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
 import "./Hpage.css";
 import $, { map } from 'jquery';
 import Hpage from './Hpage';
 import Button from 'react-bootstrap/Button';
 import LearningGraphDraft from './LearningGraphDraft';
 import {Link, Routes, Route, useHistory} from 'react-router-dom';


 var listTraining =[]
 var res =[]


  
  function getAllListTraining(){
                
    var  ListTraining= {
      "async": false,
      "crossDomain": true,
      "url": "http://localhost:3000/ListTraining",
      "method": "GET",
      "headers": {
          "content-type": "application/json"
      }
    
    };
    $.ajax(ListTraining).done(function (ListTrainingData) {
    
     var ListTrainingDataObject = JSON.parse(ListTrainingData);
    // console.log('list of all training available '+ListTrainingDataObject[1].idtraining)
     

     for (var i = 0; i < ListTrainingDataObject.length; i++) {
      var value =ListTrainingDataObject[i].idtraining
      console.log('res :'+res)
      res.push(
        {idtr:value}  );
      
     }
     console.log('ListTraining :'+res)
    });
     return res;

  }
function Tpage  () {


  getAllListTraining()
  function validateForm() {
                  return duration.length > 0 ;
                 
                }

               // const [setTraining] = useState("test");
                // set value for default selection
            const [selectedValue, setSelectedValue] = useState();

            const handleChangeValue = e => {
              setSelectedValue(e.target.value);
            }

               // const handleSChange = (event) => {
             //     setTraining(event.target.value)
               // }
                let handleSelectChange = (e) =>{
                  //console.log("Training Selected!!");
                 // this.setState({ fruit:  });
                  console.log('value selcted '+e.target.value)
                }
               
                const history = useHistory();
                 
               // const [training, settraining] = useState("");
                const [duration, setduration] = useState("");
                
              let handleSubmittest = async (e) => {
                e.preventDefault();
                try {
                  let res = await fetch("https://httpbin.org/post", {
                    method: "POST",
                   body: JSON.stringify({
                   
                      duration: duration,
                   //   training:training
                    
                    }),
                    
                  });

                  console.log("resJson body :" +res.body)

                  let resJson = await res.json();
                  if (res.status === 200) {
                  //  settraining ("");
                    setduration("");
                    console.log("resJson :" +res.response)
                    history.push("/Hpage");
                   
                    
                  } else {
                     console.log("Erreur")
                  }
                } catch (err) {
                  console.log(err);
                }
              };
             

              let handleSubmit = async (e) => {
               
               //history.push("/LearningGraphDraft");
                history.push({ 
                  pathname: '/LearningGraphDraft',
                  state: { data: selectedValue }
              })
              var ws = new WebSocket("ws://localhost:3010");
              ws.addEventListener("open", () =>{
                console.log("demande de Parcours ");
                ws.send(selectedValue);
              });
               
              ws.addEventListener('message', function (event) {
                  console.log(event.data);
                  });
    console.log("the selected training from Training Page : " +selectedValue)
              }

              
            //
             // render() {  
              
                return (  
               
                  <div>
                    <Navbar bg="light" expand="lg">
                  <Container fluid>
                    <Navbar.Brand href="#">Spac-El</Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll">
                      <Nav
                        className="me-auto my-2 my-lg-0"
                        style={{ maxHeight: '90px' }}
                        navbarScroll
                      >
                    <Nav.Link href="/Portfolio">Portfolio</Nav.Link> 
                    <Nav.Link href="/">Contact</Nav.Link>
                      </Nav>
                      <Nav.Link href="/">Contact</Nav.Link>
                     
                    </Navbar.Collapse>
                  </Container>
                </Navbar> 

              <Container>  
                  <h1>
                    Choose Training
                  </h1>
                  <Row>
                    <Col xs={6}><img alt="img" width="370" height="580" src={require('./logo.png')} />
                   </Col>
                    <Col  >
                    <Form onSubmit={handleSubmit}>
                  <Form.Group className="mb-3" >
                  <h1>
                  Get Training
                  </h1>
         
        <Form.Label>Duration</Form.Label>
        <Form.Control type="txt" placeholder="0 minute " value={duration} onChange={(e) => setduration(e.target.value) } />
       </Form.Group>
      <Form.Group className="mb-3"> 
        <Form.Label>Training </Form.Label>
          <Form.Select defaultValue="Choose Training" onChange={handleChangeValue}>
         
         {res.map((res) => (
              <option value={res.idtr}>{res.idtr}</option>
            ))}


          </Form.Select>
      </Form.Group>
      <Button variant="primary" type="submit" disabled={!validateForm()}>
        Valider
      </Button>
      
    </Form>
    
       </Col>
                  </Row>
                  
                </Container>
                </div>      
                )  
              }  
         //   }  
            
        export default Tpage;