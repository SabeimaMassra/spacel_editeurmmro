import React, { useState, useEffect } from 'react';
import ReactDOM from "react-dom";
import Graph from "react-graph-vis";
import $, { map } from 'jquery';
import { nodes } from './datajs';
import{Navbar,Container,Nav,NavDropdown,FormControl}  from 'react-bootstrap'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useLocation,useHistory } from "react-router-dom";
 
const options = {
  layout: {
    hierarchical: false
  },
  edges: {
    color: "#000000"
  }
};


var LENGTH_MAIN = 350,
  LENGTH_SERVER = 150,
  LENGTH_SUB = 50,
  WIDTH_SCALE = 1,
  GREEN = "green",
  RED = "#C5000B",
  ORANGE = "orange",
  //GRAY = '#666666',
  GRAY = "gray",
  BLACK = "#2B1B17";

function randomColor() {
  const red = Math.floor(Math.random() * 256).toString(16).padStart(2, '0');
  const green = Math.floor(Math.random() * 256).toString(16).padStart(2, '0');
  const blue = Math.floor(Math.random() * 256).toString(16).padStart(2, '0');
  return `#${red}${green}${blue}`;
}
var nodestest;
var listOfAllLos=[]
//var arrayPreviousLo=[]
var finalGraphEdges = [];
var finalGraphNodes= [];
var finalGraph ;
   
      class GraphDraw {
        // defining vertex array and
        // adjacent list
        constructor(noOfVertices)
        {
          this.noOfVertices = noOfVertices;
          this.AdjList = new Map();
        }
      
        // functions to be implemented
      
        // addVertex(v)
        // add vertex to the graph
      addVertex(v)
      {
        // initialize the adjacent list with a
        // null array
        this.AdjList.set(v, []);
      }
      
        // addEdge(v, w)
        // add edge to the graph
      addEdge(v, w)
      {
        // get the list for vertex v and put the
        // vertex w denoting edge between v and w
        this.AdjList.get(v).push(w);
      
        // Since graph is undirected,
        // add an edge from w to v also
        this.AdjList.get(w).push(v);
      }
      //function remove 
      
        // printGraph()
        // Prints the vertex and adjacency list
      printGraph()
      {
        // get all the vertices
        var get_keys = this.AdjList.keys();
        // console.log(get_keys)
      //new varable 
      var previLo='LO1';
      var arrayPreviousLo=[]
     // var finalGraphNodes=[]
        // iterate over the vertices
        for (var i of get_keys)
      {
          var get_values = this.AdjList.get(i);
      
          
          var conc = "";

          arrayPreviousLo.push(previLo)
          arrayPreviousLo.forEach(element => {
            while(get_values.includes(element)){ 
         // console.log('foreach element in  arrayPreviousLo : ' +arrayPreviousLo);
         // console.log('elements of arrayPrevious : ' +element);
           var indexOfObject = get_values.indexOf(element);
           get_values.splice(indexOfObject, 1);
            
            }
        
          })

          for (var j of get_values)
          {
             
             conc += j + " ";
           
          }

         
          previLo=i
          //console.log('the value of previLo : ' +previLo);
          // print the vertex and its adjacency list
          console.log(i + " -> " + conc);
       
        
        }
        console.log("arrayPreviousLo : " + arrayPreviousLo);
     //fill it here
     arrayPreviousLo.forEach(element => {
    

   });

        }
      
      
        // bfs(v)
        // dfs(v)
      }

      
          
function getAllLosPrereq(){
  var  ListofCurLosWithPresqsettings= {
    "async": false,
    "crossDomain": true,
    "url": "http://localhost:3000/ListofCurLosWithPresq",
    "method": "GET",
    "headers": {
        "content-type": "application/json"
    }
  
  };
  $.ajax(ListofCurLosWithPresqsettings).done(function (losWthPrereq) {
    console.log(losWthPrereq)
   var losWthPrereqObject = JSON.parse(losWthPrereq);
   console.log(losWthPrereqObject)

    for (var i = 0; i < losWthPrereqObject.length; i++) {
  //fill the nodes 
  finalGraphNodes.push({
  id:losWthPrereqObject[i].idcurrlevello,
  label:losWthPrereqObject[i].idcurrlevello,
  color: "#bbdefb"
   });
     console.log(finalGraphNodes[i].id)

    }

    
  });
  console.log('finalGraphNodes inside  the getallcalass'+finalGraphNodes)

}


function getAllLosWithNoPrereq(){

  var  LosWithNoPrereqSettings= {
    "async": false,
    "crossDomain": true,
    "url": "http://localhost:3000/LosNoPrereq",
    "method": "GET",
    "headers": {
        "content-type": "application/json"
    } 
  };  
  $.ajax(LosWithNoPrereqSettings).done(function (LosWithNoPrereq) {
    console.log(LosWithNoPrereq)
   var LosWithNoPrereqObject = JSON.parse(LosWithNoPrereq);
   console.log(LosWithNoPrereqObject)

    for (var i = 0; i < LosWithNoPrereqObject.length; i++) {
 
     

     var strLoCurrvLevel =LosWithNoPrereqObject[i].idcurrlevello
     var strLoNextLevel =LosWithNoPrereqObject[i].idnextlo.toString()
     var RecommendedColor=GRAY
     if(strLoCurrvLevel=='LO1' &&strLoNextLevel=='LO2')
     {
     RecommendedColor=RED
      
     }
     finalGraphEdges.push({
      from:strLoCurrvLevel,
      to: strLoNextLevel,
      color: RecommendedColor,
      width: WIDTH_SCALE * 2,
     // label: "is Prerequisite LO",
     
    });

    //console.log(finalGraphNodes[i].id)

    }
    

  });
 console.log('final graph nodes after getting all Lo with no prerequisit')
}

//post the progress of the user 
function insertPath(){
//get user progress
  $.ajax({
    "async": true,
    "crossDomain": true,
    url:"http://localhost:3000/insertpath",
    type:"post",
    dataType:'json', 
    data:{
      User:'',
      LosSeen:{
        //list of all seen LO and Prs
      },

     },
}).done(function (res) {
  
}) 

}

function getLevelByLos(SelectedTraining)  {
  
console.log("inside get getLevelByLos " +SelectedTraining);

//console.log('the value of the selcted tr befor '+SelectedTraining)

  var  listlosettings= {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:3000/listlo",
    type:"post",
    dataType:'json', 
    data:{
    tr:'DEV' //SelectedTraining
   },

  //  "method": "GET",
   // "headers": {
    //    "content-type": "application/json"
    //}
  
  };
 

$.ajax(listlosettings).done(function (graph) {
console.log(graph)
var loarray = JSON.parse(graph);

//fill with all los
loarray.forEach(element => {

  var Lo=element.idcurrlevello
  listOfAllLos.push(Lo)
  listOfAllLos.push(element.idnextlo)


});
//only unique value
function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

// usage example of unique value
var a = ['a', 1, 'a', 2, '1'];
var unique = a.filter(onlyUnique);

console.log(unique); // ['a', 1, 2, '1']
//to group by id previous
const result = loarray.reduce((group, product) => {
  const { idcurrlevello } = product;
  group[idcurrlevello] = group[idcurrlevello] ?? [];
  group[idcurrlevello].push(product);
  return group;
}, {});
console.log(' result '+result);

//get the length of the array 
var g = new GraphDraw(10);

for (var i = 0; i < listOfAllLos.length; i++) {
	g.addVertex(listOfAllLos[i]);
}

for (var i = 0; i < loarray.length; i++) {



  var strLoCurrvLevel =loarray[i].idcurrlevello
  var strLoNextLevel =loarray[i].idnextlo.toString()

//a condition for testing
  var colorByUser=GRAY;
  if(strLoCurrvLevel=='LO2' && strLoNextLevel=='LO3'
     ||strLoCurrvLevel=='LO3' && strLoNextLevel=='LO5'
     ||strLoCurrvLevel=='LO5' && strLoNextLevel=='LO7'
     ||strLoCurrvLevel=='LO7' && strLoNextLevel=='LO9'){
 
  var colorByUser=RED;

  }
//end test
 console.log(strLoCurrvLevel+','+strLoNextLevel)
  //console.log('the next index is : '+strLoNextLevel)

	g.addEdge(strLoCurrvLevel, strLoNextLevel);
 //get the edged here 
  finalGraphEdges.push({
  from:strLoCurrvLevel,
  to: strLoNextLevel ,
  width: WIDTH_SCALE * 2,
  color: colorByUser,
  //label: "is Prerequisite LO",
 
});
console.log('edges  from  '+finalGraphEdges[i].from +" :"+finalGraphEdges[i].to)


}
//for test
//finalGraphEdges.push({
 // from:'LO0',
 // to: 'LO8'
//});

g.printGraph();

});
 nodestest=[
  { id: 1, label: '1' },
  { id: 2, label: '2' },

]
console.log('inside test '+finalGraphNodes)

}

 function getLoPrs(lo){
  
   console.log('get LoPrs was cliked : '+lo)
    $.ajax({
        "async": false,
        "crossDomain": true,
        url:"http://localhost:3000/getPrs",
        type:"post",
        dataType:'json', 
        data:{
          nameLo:lo
         },
    }).done(function (Resdata) {
      //var res = JSON.parse(Resdata);
     // console.log("getLoPrs/list of learning objects ");
    })      
    

 }
 var SelectedTraining 
  
function testvalue (){
  
  console.log("the value of variable slected training in test value "+SelectedTraining);
  
}

function LearningGraphDraft(props)  {
 
console.log("the data passed from Training page "+props.history.location.state.data);
SelectedTraining =props.history.location.state.data;
SelectedTraining='DEV'
testvalue();
getLevelByLos(SelectedTraining) 
getAllLosPrereq(SelectedTraining)
getAllLosWithNoPrereq(SelectedTraining)


console.log('finalGraphNodes inside Graph :'+finalGraphNodes)
  //const App = () => {
    const createNode = (x, y) => {
    const color = randomColor();
      
      setState(({ graph: { nodes, edges }, counter, ...rest }) => {
        const id = counter + 1;
        const from = Math.floor(Math.random() * (counter - 1)) + 1;
        return {
          graph: {
            nodes: [
              ...nodes,
              { id, label: `Node ${id}`, color, x, y }
            ],
            edges: [
              ...edges,
              { from, to: id }
            ]
          },
          counter: id,
          ...rest
        }
      });
    }

const [state, setState] = useState({
    counter: 18,
    graph:{
      nodes:finalGraphNodes
    
,
       edges: 
     finalGraphEdges
    },
     
    events: {
      
      select: ({ nodes, edges }) => {
    
        //get ressources for a LO
        getLoPrs(nodes)
       
        alert("Selected node: " + nodes);
      },
      doubleClick: ({ nodes ,pointer: { canvas } }) => {
      
       
        //createNode(canvas.x, canvas.y);
      }
    }
  })

  const { graph, events } = state;
 
  return (
    <div>
    <Navbar bg="light" expand="lg">
    <Container fluid>
      <Navbar.Brand href="#">Spac-El</Navbar.Brand>
      <Navbar.Toggle aria-controls="navbarScroll" />
      <Navbar.Collapse id="navbarScroll">
        <Nav
          className="me-auto my-2 my-lg-0"
          style={{ maxHeight: '90px' }}
          navbarScroll
        >
      <Nav.Link href="/Hpage">Home</Nav.Link>
      <Nav.Link href="/Portfolio">Portfolio</Nav.Link> 
        </Nav>

        <Nav.Link href="/">Contact</Nav.Link>
       
      </Navbar.Collapse>
    </Container>
  </Navbar> 
 
  <Container>
  <h1>
                    Spac-eL
                  </h1>
  <Row>
                    <Col xs={6}><img alt="img" width="370" height="580" src={require('./logo.png')} />
                   </Col>
                   <Col >      
     <h1>
                    Learning Graph for DEV Training 
                  </h1>
    <Graph graph={graph} options={options} events={events} style={{ height: "640px" }} />
    
    
                    </Col>
                  </Row></Container>
    </div>
    );
  

//}

}
export default LearningGraphDraft;