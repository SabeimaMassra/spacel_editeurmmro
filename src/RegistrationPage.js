import { useState } from 'react';
import React from 'react' 
import { Grid, Image } from 'semantic-ui-react' 

import{Navbar,Container,Nav,NavDropdown,FormControl}  from 'react-bootstrap'
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import {Link, Routes, Route, useHistory} from 'react-router-dom';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import Card from 'react-bootstrap/Card';


           function RegistrationPage() {
   
                return (  
                
                  <Container  maxWidth="550px" maxHeight ="100px" >
                    <Navbar bg="light" expand="lg">
                  <Container >
                    <Navbar.Brand href="#">Spac-El</Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll" />
                    <Navbar.Collapse id="navbarScroll">
                      <Nav
                        className="me-auto my-2 my-lg-0"
                        style={{ maxHeight: '90px' }}
                        navbarScroll
                      >
                    <Nav.Link href="/Home">Home</Nav.Link>
                      </Nav>

                      <Nav.Link href="/LearningGraphDraft">LearningGraph</Nav.Link>
                     
                    </Navbar.Collapse>
                  </Container>
                </Navbar> 
                <Row>
                <Col  >

              
             
             <Form>
      <Row className="mb-3">
        
        <Form.Group as={Col} controlId="formGridEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" placeholder="Enter email" />
        </Form.Group>

        <Form.Group as={Col} controlId="formGridPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" />
        </Form.Group>
      </Row>

      <Form.Group className="mb-3" controlId="formGridAddress1">
        <Form.Label>Address</Form.Label>
        <Form.Control placeholder="1234 Main St" />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formGridAddress2">
        <Form.Label>Address 2</Form.Label>
        <Form.Control placeholder="Apartment, studio, or floor" />
      </Form.Group>

      <Row className="mb-3">
        <Form.Group as={Col} controlId="formGridCity">
          <Form.Label>City</Form.Label>
          <Form.Control />
        </Form.Group>

        <Form.Group as={Col} controlId="formGridState">
          <Form.Label>State</Form.Label>
          <Form.Select defaultValue="Choose...">
            <option>Choose...</option>
            <option>...</option>
          </Form.Select>
        </Form.Group>

        <Form.Group as={Col} controlId="formGridZip">
          <Form.Label>Zip</Form.Label>
          <Form.Control />
        </Form.Group>
      </Row>

      <Form.Group className="mb-3" id="formGridCheckbox">
        <Form.Check type="checkbox" label="Check me out" />
      </Form.Group>

      <Button variant="primary" type="submit">
        Submit
      </Button>
    </Form>  
                </Col>
                
                </Row> 
                </Container>      
                )  
              }  
         //   }  
            
        export default RegistrationPage;